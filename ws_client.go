package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  cfg.Misc.MaxMsgSize * 2,
	WriteBufferSize: cfg.Misc.MaxMsgSize * 2,
}

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan Message
	ip   string
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.hub.broadcast <- getOnlineMessage(getClientCount(c.hub) - 1)
		err := c.conn.Close()
		if err == nil {
		}
	}()
	c.conn.SetReadLimit(int64(cfg.Misc.MaxMsgSize))
	err := c.conn.SetReadDeadline(time.Now().Add(pongWait))
	if err != nil {
		log.Printf("Can't set readdeadline: %e", err)
	}
	c.conn.SetPongHandler(func(string) error {
		err = c.conn.SetReadDeadline(time.Now().Add(pongWait))
		if err != nil {
			log.Printf("Can't set readdeadline: %e", err)
		}
		return nil
	})
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}

		m, err := DecodeMsg(message)
		if err != nil {
			log.Printf("Can't decode message")
			continue
		}
		handleMessage(m, c)
	}
}

func handleMessage(m *Message, c *Client) {
	switch m.Command {
	case MESSAGE:
		saveAndBroadcast(m, c)
	case GET_OLD_MESSAGES:
		sendOldMessages(c, m.Id)
	case GET_MESSAGE_BY_ID:
		sendMessageById(c, m.Id)
	}
}

func sendMessageById(c *Client, id int64) {
	message := db.getMessageById(id)
	message.Command = GET_MESSAGE_BY_ID
	c.send <- message
}

// sendOldMessages sends N msgs(defined in config file) to X client.
// Currently, it sends messages on GET_OLD_MESSAGES command or when client just connects
func sendOldMessages(c *Client, offset int64) {
	messages := db.getOldMessages(cfg.Misc.NumOldMsgs, offset)
	for i := len(messages) - 1; i > 0; i-- {
		messages[i].Command = GET_OLD_MESSAGES
		c.send <- messages[i]
	}
}

// saveAndBroadcast saves message struct in database and send this to every client.
// It also prints message to stdout.
func saveAndBroadcast(m *Message, c *Client) {
	m.sender = c
	m.Time = time.Now().Unix()
	id, err := db.saveMessage(m)
	if err != nil {
		log.Print(err)
	}
	m.Id = id
	m.log()
	c.hub.broadcast <- *m
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		err := c.conn.Close()
		if err == nil {
		}
	}()
	for {
		select {
		case message, ok := <-c.send:
			err := c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err != nil {
				log.Printf("Can't set writedeadline: %e", err)
			}
			if !ok {
				// The hub closed the channel.
				_ = c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				return
			}

			message.Mine = message.sender == c
			m, err := message.EncodeMsg()
			if err != nil {
				log.Printf("Can't encode message")
				continue
			}
			_, err = w.Write(m)
			if err != nil {
				log.Printf("Can't write a message to client: %e", err)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			err := c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err != nil {
				log.Printf("Can't set writedeadline: %e", err)
			}
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// newWSClient handles websocket requests from the peer.
func newWSClient(w http.ResponseWriter, r *http.Request, hub *Hub) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	client := &Client{hub: hub, conn: conn, send: make(chan Message)}
	client.ip = r.Header.Get("X-Forwarded-For")
	if len(client.ip) == 0 {
		client.ip = client.conn.RemoteAddr().String()
	}
	client.hub.register <- client
	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()

	client.sendLastMsgs()
	changeOnlineCounter(hub)
}

func changeOnlineCounter(hub *Hub) {
	for client := range hub.clients {
		select {
		case client.send <- getOnlineMessage(getClientCount(hub)):
		}
	}
}

func getClientCount(hub *Hub) int64 {
	return int64(len(hub.clients))
}

func getOnlineMessage(count int64) Message {
	return Message{Id: count, Command: UPDATE_ONLINE}
}

func (c *Client) sendLastMsgs() {
	messages := db.getOldMessages(cfg.Misc.NumOldMsgs, 0)
	for _, m := range messages {
		c.send <- m
	}
}

func processWeb() {
	defer wg.Done()
	hub := newHub()
	go hub.run()

	mux := http.NewServeMux()
	setWSRoute(mux, hub)
	setStaticRoutes(mux)
	mux.HandleFunc("/nojs", func(w http.ResponseWriter, r *http.Request) {
		setNoJsRoute(w, r, hub)
	})

	addr := fmt.Sprintf("%s:%d", cfg.Server.Host, cfg.Server.Port)
	log.Printf("Starting Http-server addr='http://%s:%d'", cfg.Server.Host, cfg.Server.Port)
	err := http.ListenAndServe(addr, mux)
	if err != nil {
		log.Fatal(err)
	}
}

func setNoJsRoute(w http.ResponseWriter, r *http.Request, hub *Hub) {

	switch r.Method {
	case http.MethodPost:
		msgbody := r.PostFormValue("msgbody")
		if len(msgbody) == 0 {
			log.Printf("NoJs: Empty message")
			return
		}
		m := &Message{
			Text:  msgbody,
			Name:  "",
			Board: "",
			Time:  time.Now().Unix(),
			Id:    0,
		}
		id, err := db.saveMessage(m)
		if err != nil {
			log.Print(err)
		}
		m.Id = id
		m.sender = &Client{ip: r.RemoteAddr}
		m.log()
		hub.broadcast <- *m
		http.Redirect(w, r, "/nojs", http.StatusSeeOther)
	case http.MethodGet:
		renderNoJs(w, hub)
	}
}

type NoJsTmpl struct {
	Log           []Message
	OnlineCounter int64
}

func renderNoJs(w http.ResponseWriter, h *Hub) {
	tmpl := template.Must(template.ParseFiles("static/nojs_home.html"))
	messages := db.getOldMessages(cfg.Misc.NumOldMsgs, 0)
	for i := 0; i < len(messages); i++ {
		t := time.Unix(messages[i].Time, 0)
		messages[i].Datetime = t.Format(time.TimeOnly)
	}
	err := tmpl.Execute(w, NoJsTmpl{messages, getClientCount(h) + 1})
	if err != nil {
		log.Printf("unable to send templ: %s\n", err)
	}
}

func setStaticRoutes(mux *http.ServeMux) {
	routeStaticFile(mux, "/", "static/home.html")
	routeStaticFile(mux, "/robots.txt", "robots.txt")

	mux.HandleFunc("/logs/", func(w http.ResponseWriter, r *http.Request) {
		returnMethodError(w, r)
		handleLogs(w, r)
	})
	mux.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		returnMethodError(w, r)
		http.ServeFile(w, r, path.Dir("")+r.URL.Path)
	})
}

func setWSRoute(mux *http.ServeMux, hub *Hub) {
	mux.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		newWSClient(w, r, hub)
	})
}
