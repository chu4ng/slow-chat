# Setting up fastcgi: 
```
server {
    server_name  slow.ch;
    listen 80;
    listen 443 ssl http2;
    ssl_certificate /etc/ssl/certs/slow.ch.cert;
    ssl_certificate_key /etc/ssl/private/slow.ch.key;

    location / {

        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Real-IP $remote_addr;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_pass http://127.0.0.1:8081;
    }
}
