window.onload = function () {
    const next = document.getElementById("next");
    const prev = document.getElementById("previous");

    for (let elem of document.getElementsByClassName("msgcontent")) {
        elem.innerHTML = markdownParser(elem.innerText)
    }
    for (let elem of document.getElementsByClassName("msgtime")) {
        elem.innerHTML = getTime(new Date(elem.innerText * 1000))
    }

    next.addEventListener("click", function () {
        window.location = (changeDate(1).toISOString().split("T")[0])

    })
    prev.addEventListener("click", function () {
        window.location = (changeDate(-1).toISOString().split("T")[0])
    })

    document.getElementById("stylechange").addEventListener("click", switchStyle)
};

function changeDate(date) {
    let d = getCurrentDate()
    d.setDate(d.getDate() + date);
    return d
}

function getCurrentDate() {
    return new Date(window.location.pathname.replace(/\/logs\//, ""))
}
