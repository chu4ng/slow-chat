const MessageObject = {
    Text: "",
    Name: "",
    Board: "",
    Time: 0,
    Id: 0,
    Command: 0,
    Mine: false,
};

const commonIcon = "/static/favicon.ico"
const messedIcon = "/static/favicon_mess.ico"
const maxMsgSizeInByte = 2000

const MsgType = {
    MESSAGE           : 0,
    UPDATE_ONLINE     : 1,
    GET_OLD_MESSAGES  : 2,
    GET_MESSAGE_BY_ID : 3
}
const PreviewLifetime = 2*1000
// TODO replace message-codes with constants.
window.onload = function () {
    if (window.location.pathname !== "/") {
        return
    }
    let conn;
    const msg = document.getElementById("msg");
    const name = document.getElementById("name");
    const onlineCount = document.getElementById("onlineCount");
    const log = document.getElementById("log");
    let mineLS = new MineLS()
    // TODO make new class

    function previewMessageOnTable(message) {
        message.classList.add("highlightRef")
        setTimeout(() => {
            message.classList.remove("highlightRef")
        }, PreviewLifetime)
    }

    function sendPreviewMsgCommand(messId, parentId, parentElem) {
        let m = MessageObject;
        m.Command = MsgType.GET_MESSAGE_BY_ID
        m.Id = parseInt(messId)
        let b = MessagePack.encode(m)
        conn.send(b);
    }

    window.tryPreviewMessage = function (messId, parent) {
        let message = getMessageOnTheTable(messId)
        if (message !== false) {
            previewMessageOnTable(message)
        } else {
            sendPreviewMsgCommand(messId, parseInt(parent.innerText.replace(/>>/, "")), parent);
        }
    }

    function getMessageOnTheTable(messId) {
        for (const mesElem of document.getElementsByClassName("messageContainer")) {
            if (mesElem.getElementsByClassName("msgid")[0].innerText === messId.toString()) {
                return mesElem
            }
        }
        return false
    }

    function previewMessage(mes) {
        let mesElem = createMessageElem(mes)
        if (mesElem === false) {
            return
        }
        let parentMsg = getMessageOnTheTable(mes.Id)
        let parentRef
        if (parentMsg === false) {
            return
        }
        for (const elem of parentMsg.getElementsByClassName("response")) {
            if (elem.innerText === mes.Id.toString()) {
                parentRef = elem
            }
        }
        if (parentRef === undefined) {
            return
        }
        parentRef.onmouseleave = () => {
            // Make it better idiot
            mesElem.remove()
        }
        log.append(mesElem)
    }

    // Load old messages
    document.getElementById("loadold").onclick = function () {
        let m = MessageObject;
        let firstId = log.children[0].getElementsByClassName("msgid")[0].innerText
        m.Command = MsgType.GET_OLD_MESSAGES
        m.Id = parseInt(firstId)
        m.Text = ""
        let b = MessagePack.encode(m)
        conn.send(b);
    }

    // Send message
    document.getElementById("form").onsubmit = function () {
		sizeDiff = maxMsgSizeInByte - new Blob(Array(msg.value)).size 
        if (!conn || !msg.value.trim()) {
            return false;
        } if (sizeDiff < 0) {
            // TODO show error about too long msg
            createInfoMessage("Message input is too long(for about " + -sizeDiff + " bytes).", 3)
            return false
		}
        let m = MessageObject;
        m.Text = msg.value
        m.Name = name.value
        m.Command = MsgType.MESSAGE
        let b = MessagePack.encode(m)
        conn.send(b);
        msg.value = "";
        return false;
    };

    function pasteReply() {
        let buf = ">>" + this.innerText + " "
        msg.value = (msg.value === "") ? buf : msg.value + "" + buf
        msg.focus()
    }

    function createInfoMessage(text, lifetime) {
        let userMsg = msg.value
        let oldColor = msg.style.color

        msg.value = text
        msg.readOnly = true
        msg.style.color = "red"
            setTimeout(() => {msg.value = userMsg; msg.readOnly=false; msg.style.color = oldColor}, lifetime*1000)
    }

    function createMessageElem(mes) {
        if (mes.Id === 0) {
            return
        }
        let messageContainer = document.createElement("div");
        messageContainer.classList.add("messageContainer")
        let id = document.createElement("span");
        id.innerText = mes.Id
        id.onclick = pasteReply
        id.classList.add("msgid")
        let idpost = document.createElement("span");
        let idpre = document.createElement("span");
        idpre.innerText = "<"
        idpost.innerText = ">"
        messageContainer.appendChild(idpre)
        messageContainer.appendChild(id)
        messageContainer.appendChild(idpost)
        let time = document.createElement("span");
        time.classList.add("msgtime")
        let date = new Date(mes.Time * 1000)
        time.innerText = "[" + getTime(date) + "]"
        time.title = getDate(date)
        messageContainer.appendChild(time)
        let name = document.createElement("span");
        name.classList.add("msgname")
        name.innerHTML = markdownParser(mes.Name)
        messageContainer.appendChild(name)
        let textitem = document.createElement("div");
        let messagetext = document.createElement("span");

        if (mes.Mine) {
            mineLS.add(mes.Id)
            mineLS.save()
        }
        textitem.classList.add("split")
        textitem.classList.add((mineLS.has(mes.Id)) ? "minesplit" : "otherssplit")

        messagetext.classList.add("msgcontent")
        messagetext.innerHTML = markdownParser(mes.Text)
        textitem.appendChild(messagetext)
        messageContainer.appendChild(textitem)
        return messageContainer
    }

    function pushSimpleMessageToLog(msg) {
        let item = document.createElement("div");
        item.innerHTML = "<b>" + msg + "</b>";
        appendAndFocus(item)
    }

    function appendAndFocus(elem) {
        log.append(elem);
        unfocusAddNotify()
        elem.scrollIntoView()
    }

    function handleMessage(mes) {
        let messageElement
        switch (mes.Command) {
            case MsgType.MESSAGE:
                messageElement = createMessageElem(mes)
                if (messageElement !== undefined) {
                    appendAndFocus(messageElement)
                }
                break
            case MsgType.UPDATE_ONLINE:
                onlineCount.innerText = "Online: " + mes.Id
                break
            case MsgType.GET_OLD_MESSAGES:
                messageElement = createMessageElem(mes)
                if (messageElement !== undefined) {
                    log.prepend(messageElement)
                }
                break
            case MsgType.GET_MESSAGE_BY_ID:
                previewMessage(mes)
                break
            default:
                break
        }
    }

    function connectWS() {
        if (document.location.protocol === "https:") {
            conn = new WebSocket("wss://" + document.location.host + "/ws");
        } else {
            conn = new WebSocket("ws://" + document.location.host + "/ws");
        }
        conn.binaryType = "arraybuffer"
        conn.onclose = function (_) {
            pushSimpleMessageToLog("Connection closed.")
        };
        conn.onmessage = function (evt) {
            let mes = MessagePack.decode(evt.data)
            handleMessage(mes);
        };
    }

    if (window["WebSocket"]) {
        connectWS();
    } else {
        pushSimpleMessageToLog("Your browser does not support WebSockets.")
    }

    function switchNameInput(e) {
        if (e.altKey && e.shiftKey && e.which === 78) {
            name.hidden = !name.hidden
        }
    }
	function inputAddNewLine(e) {
		if( e.shiftKey && e.key == "Enter") {
			e.preventDefault()
			msg.value = msg.value + "\\n"
		}
	}

    document.addEventListener("keyup", switchNameInput)
    document.addEventListener("keydown", inputAddNewLine)
    document.getElementById("stylechange").addEventListener("change", switchStyle)
};

// Temporarily. Replace with split-line like it was in pychat.
function getDate(date) {
    return String(date.getDate()).padStart(2, "0") + "." +
        String(date.getMonth() + 1).padStart(2, "0")+ "." +
        String(date.getFullYear()).padStart(4, "0")
}

function getTime(date) {
    return String(date.getHours()).padStart(2, "0") + ":" +
        String(date.getMinutes()).padStart(2, "0")+ ":" +
        String(date.getSeconds()).padStart(2, "0")
}

function markdownParser(text) {
    toHTML = text
		.replace(/\\n$/g, "")
        .replace(/(>>)([0-9]+)/gim, '<a onmouseenter="tryPreviewMessage($2, this)" class="response">$1$2</a>')
		.replace(/(https?:\/\/[^\s\[\]{}]+)/g, '<a target="_blank" href="$1">$1</a>')
        .replace(/^(>.*)/gim, '<span class="greentext">$1</span>')
        .replace(/%%(.*?)%%/gim, '<span class="spoiler">$1</span>')
        .replace(/\[s](.*?)\[\/s]/gim, '<span class="strike">$1</span>')
        .replace(/\[irony](.*?)\[\/irony]/gim, '<span class="irony">$1</span>')
        .replace(/\*\*(.*?)\*\*/gim, '<b>$1</b>')
        .replace(/\[b](.*?)\[\/b]/gim, '<b>$1</b>')
        .replace(/\*(.*?)\*/gim, '<i>$1</i>')
        .replace(/\[i](.*?)\[\/i]/gim, '<i>$1</i>')

	linesArray = toHTML.split("\\n")
	htmlArray = []
	for(m of linesArray) {
		htmlLine = "<span>" + m + "</span>"
		htmlArray.push(htmlLine)
	}
	splitText = htmlArray.join("<br>")
    return splitText.trim();
}

function changeStyle(style) {
    let theme = document.getElementById('stylelink');
    theme.setAttribute('href', style);
    localStorage.setItem("style", style)
}

function switchStyle(v) {
    let currentStyle = document.getElementById('stylelink')
    let newStyle = v.target.value
    if (!styles.hasOwnProperty(newStyle)) {
        console.log("Can't switch to style " + newStyle)
        return
    }
    newStyle = styles[newStyle]
    localStorage.setItem("style", newStyle)
    currentStyle.setAttribute('href', newStyle)
}

function tryToLoadStyle() {
    let old = localStorage.getItem("style")
    if (typeof old == "string" && Object.values(styles).includes(old)) {

    } else {
        console.log("Can't load style" + old + "\nSafe default")
        let currentStyle = document.getElementById('stylelink')
        old = currentStyle.getAttribute("href")
    }
    changeStyle(old)
}

const styles = {
    "pink": "/static/styles/pink.css",
    "origin": "/static/styles/origin.css",
    "night": "/static/styles/night.css",
}

function changeFavicon(src) {
        let link = document.createElement('link')
        let oldLink = getFavicon()
        link.id = 'dynamic-favicon';
        link.rel = 'shortcut icon';
        link.href = src;
        if (oldLink) {
            document.head.removeChild(oldLink);
        }
        document.head.appendChild(link);
}

function unfocusAddNotify() {
    if (!document.hasFocus()) {
        changeFavicon(messedIcon)
    }
}

function focusDelNotify() {
    let iconElem = getFavicon()
    if (iconElem.href.replace(window.location.origin, "") === messedIcon) {
        changeFavicon(commonIcon)
    }
}

function getFavicon() {
    return document.getElementById('dynamic-favicon')
}

class MineLS {
    constructor() {
        let mine = localStorage.getItem("mine")
        if (mine === null) {
            this.setNull()
        } else {
            this.mine = mine.split(',')
            if (this.mine.length > 10000) {
                this.setNull()
            }
        }
    }
    save() {
        localStorage.setItem("mine", this.mine.join())
    }
    has(id) {
        return this.mine.map(i => parseInt(i)).indexOf(id) !== -1
    }
    add(id) {
        this.mine.push(String(id))
    }
    setNull() {
        this.mine = ["0"]
        localStorage.setItem("mine", "0")
    }
}

window.addEventListener("load", tryToLoadStyle)
window.addEventListener('focus', focusDelNotify);
