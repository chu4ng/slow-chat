module simple-chat

go 1.17

require (
	github.com/gorilla/websocket v1.5.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	github.com/vmihailenco/msgpack/v5 v5.3.5
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
