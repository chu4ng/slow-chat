package main

import (
	"net/http"
	"sync"
)

// TODO allowed "boards"
// TODO Mod page etc

var db *Database
var cfg Config
var wg = &sync.WaitGroup{}

func main() {
	cfg = readConfig()
	db = initDB()
	dumpLastNMsgs()

	// Also, I'm planning add again tcp support and another, so without waitGroup it could look not cool.
	wg.Add(1)
	go processWeb()
	wg.Wait()
}

func dumpLastNMsgs() {
	messages := db.getOldMessages(5, 0)
	for _, m := range messages {
		m.log()
	}
}

func routeStaticFile(mux *http.ServeMux, inPath string, outPath string) {
	mux.HandleFunc(inPath, func(w http.ResponseWriter, r *http.Request) {
		returnMethodError(w, r)
		http.ServeFile(w, r, outPath)
	})
}

func returnMethodError(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
}
