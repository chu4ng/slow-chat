# Slowchat

It is a simple web-chat that use WebSocket and MsgPack.

Currently available only one room.
There's no any identification on server side, but users can enter some username.
For now chat use postgres, but other DB can be added in feature.

## Features
- logs page available
- several color-schemas
- yaml config
- MsgPack
- Postgres
- Docker/compose support

## Build'n'run
```
go build
./simple-chat;

```

.service file attached for running as a daemon.

notes.md contain config example for nginx set up.

### Upcoming features
- [ ] Messages preview
- [ ] Customized image/video preview
- [ ] Any other DB
- [ ] Graceful restart  (https://gist.github.com/rivo/f96ad8710b54a49180a314ec4d68dbfb for example)
